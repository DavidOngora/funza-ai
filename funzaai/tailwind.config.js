/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**"],
  theme: {
    extend: {
      colors: {
        darkgray: "#212121",
        body: "#5C5C5C",
        solid: "#FF8A00",
        fadient: "linear-gradient(253.58deg, #FFC000 1.55%, #FF8A00 95.8%)",
      },
    },
  },
  plugins: [],
};
