import './App.css';
import Navbar from './Components/Navbar'
import Learn from './Components/Learn'
import Footer from './Components/Footer'
import Smarter from './Components/Smarter';
import NewsLetter from './Components/NewsLetter';
// import Pricing from './Components/Pricing'

function App() {
  
  return (
    <>
    <div>     
      < Navbar />
      < Learn />
      {/* < Pricing /> */}
      < NewsLetter />
      < Smarter />
      < Footer />
    </div>
    </>
  );
}

export default App;
