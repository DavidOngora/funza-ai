import React from 'react';
import five from '../assets/five.png';
import Button from '../Container/Button';

const Smarter = () => {
    
  return (
    <div className='flex'>
      <img src={five} alt="" />
      <div className="grid ml-8  ">
      <span className='text-fadient font-bold text-4xl mt-20'>Learn Smarter, <br />Not Harder <br />with FunzAI</span>
      <span className='text-body -mt-8' >FunzAI is an innovative educational platform that combines 
            the power of artificial intelligence and blockchain technology 
            to enhance learning. With a vast library of courses, 
            FunzAI offers ineractive, personalized learning experiences
            tailored to each learner’s unique needs. 
            Earn blockchain-backed certificates, badges, and rewards
            as you advance your knowledge adn skills.</span>
            < Button description="Enroll Now" />
    </div>
    
    </div>
  );
};

export default Smarter;
