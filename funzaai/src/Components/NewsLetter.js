import React from 'react'
import Button  from '../Container/Button'

const NewsLetter = () => {
  return (
    <>
    <div className='flex space-x-20 mt-24 ml-32 mb-32'>
      <div className="grid">
        <span className="font-bold text-[24px]">Suscribe to </span>
        <span className="font-bold text-[24px] mb-8">Our Newsletter</span>

        <span>Get exclusive discounts and latest news
             deliverd to your inbox for free!</span>
        </div>          
             <div className="relative flex mt-12">
              {/* <label htmlFor=""></label> */}
              <input className="border-2 h-[56px] w-[523px] rounded-full pl-3 shadow-md" type="email" placeholder="Email" />
                < Button className="ml-18" description="Submit" />
             
             </div>
             </div>
             </>
  )
}

export default NewsLetter