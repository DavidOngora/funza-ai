import React from 'react'

const User = () => {
  return (
    <>
    < Navbar />
    <div>
        <span>Bosun Jones</span>
        <span>Age: 28</span>
        <span>Occupation: Data Analyst</span>
    </div>
    <div className="">
        <span>
            <b>Learning Goals</b>

Master AI for career advancement.
Explore blockchain technology.
Enhance leadership skills.</span>
    </div>
    <div className="">
        <span>Learning Progress:</span>
        <span>Completed: 3 courses
In Progress: 1 course</span>
    </div>
    <div className="">
        <span>Current course progress: 60% complete</span>
        {/* progress bar */}
        <span>Blockchain Fundamentals</span>
        <span>Continue Course</span>
        <span>You've earned the 
Enthusiast Badge!</span>
    </div>
    {/* cards */}
    < Footer />
    </>
  )
}

export default User