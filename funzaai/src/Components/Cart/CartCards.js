import React from 'react'

const CartCards = ({course, instructor, Modules, date, price, subtotal, Delivery, Total }) => {
    const [val, setVal] = useState(false)

    const Checkbox = (props) => {
        return (
            <>
            <label className="checkbox">
      <input
        type="checkbox"
        name={props.name}
        checked={props.val}
        onChange={() => {
          props.setValue(!props.val);
        }}
      />
      {props.label}
    </label>
            </>
        )
    }

  return (
    <div>
        <span>Product Name & Details</span>
        <span>Price</span>
        <span>Payment Method</span>
        <hr />
        <span>Blockchain Fundamentals</span>
        < Checkbox value={val} setValue={setVal}></Checkbox>
        
        < Button description="Buy" />
    </div>
  )
}

export default CartCards