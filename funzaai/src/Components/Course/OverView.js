import React from 'react'

const OverView = () => {
  return (
    <>
    < Navbar />
    <div>
        <span>You can either enroll for this course to gain access and also to the materials been
             attached to it online only or you can add to cart for checkout to view offline</span>
    </div>
    < Button description="Become Premium" />
    < Button description="Add to cart" />
    <div className="block">
        <span>Blockchain Fundamentals</span>
    </div>
    <div className="tags">
        <span>Overview</span>
        <span>Transcript</span>
        <span>Offline Package</span>
        <span>Exercise Files</span>
    </div>
    <div className="">
        <div className="instructor">
            <span>Instructor</span>
            <span>John Doe</span>
            <span>John Doe is a senior Blockchain specialist at Coinbase. He also teaches Blockchain
                 fundamentals on a weekly basis to the community and also has piloted other programs for institutions.</span>
                 {/* socials */}
        </div>
    </div>
    <div className="description">
        <span>Description</span>
        <span>Embark on a journey into the revolutionary world of blockchain technology with our "Blockchain Fundamentals" course.
             Discover the core principles, mechanics, and transformative potential of this decentralized ledger system that underpins cryptocurrencies
              like Bitcoin and is reshaping industries worldwide.

In this engaging course, you'll delve into the intricacies of blockchain, exploring topics 
such as distributed ledgers, consensus mechanisms, smart contracts, and real-world applications.
 Gain practical insights and hands-on experience, equipping you to navigate the rapidly evolving landscape of blockchain technology.

Join us to uncover the secrets of blockchain and unlock a world of innovation and opportunity. 
Whether you're a newcomer or an industry professional, this course provides the foundation you need to thrive in the blockchain revolution.</span>


<span>Release Date: 1/03/2023</span>
<span>Duration: 10 h 05 mins 00 secs</span>
<span>Cryptocurrency, Tokenization, Smart Contracts</span>
    </div>
    <div className="similar">
        <span>Similar Courses</span>
        {/* course cards */}
    </div>
    <div className="">
        <span>You can either enroll for this course to gain access and also to the materials been attached
             to it online only or you can add to cart for checkout to view offline</span>

             < Button description="Become Premium" />
             < Button description="Add to Cart" />
    </div>
    < NewsLetter />
    < Footer />
    </>
  )
}

export default OverView