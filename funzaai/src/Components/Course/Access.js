import React from 'react'

const OverView = () => {
  return (
    <>
    < Navbar />
    <div>
        <span>You can either enroll for this course to gain access and also to the materials been
             attached to it online only or you can add to cart for checkout to view offline</span>
    </div>
    < Button description="Become Premium" />
    < Button description="Add to cart" />
    <div className="block">
        <span>Blockchain Fundamentals</span>
    </div>
    <div className="tags">
        <span>Overview</span>
        <span>Transcript</span>
        <span>Offline Package</span>
        <span>Exercise Files</span>
    </div>
    
    <div className="download">
        <span>Download Course For Offline View</span>
        <span>You can either enroll for this course to gain access and also to the materials been attached to it online only or you can add to cart for checkout to view offline</span>
    < Button description="Add To Cart" />
    </div>
    <div className="similar">
        <span>Similar Courses</span>
        {/* course cards */}
    </div>
    <div className="">
        <span>You can either enroll for this course to gain access and also to the materials been attached
             to it online only or you can add to cart for checkout to view offline</span>

             < Button description="Become Premium" />
             < Button description="Add to Cart" />
    </div>
    < NewsLetter />
    < Footer />

    {/* popup */}
    <div className="">
        <span>Gain access to course via</span>
        <span>Access this course and the materials here on our platform and save offline if there’s a reson to.</span>
        < Button description="Subscribe Premium" />
    </div>
    </>
  )
}

export default OverView