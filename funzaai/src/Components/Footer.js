import React from 'react'
import face from "../assets/socials/1socials.png"
import twitter from "../assets/socials/2socials.png"
import youtube from "../assets/socials/3socials.png"
import binnance from "../assets/socials/4socials.png"


const Footer = () => {

  const images = [face, twitter, youtube, binnance]

  return (
    <>
    <div className='flex bg-darkgray text-white w-full '>
      <div className="ml-10 mt-28 mb-12 ">
      <span className='font-bold text-[32px] text-white mb-12'>FUNZAI </span>
      <div className="flex gap-2 mt-20">
        {images.map((image, index) => (
          <img key={index} src={image} alt={`socials $index +1`} />
        ))}
      </div>
      </div>

      <div className="ml-80 grid mt-6  mb-4">
        <span className='mb-4 p-2 font-bold '>QuickLinks</span>
        <span>Home</span>
        <span>Courses</span>
        <span>About Us</span>
        <span>Contact Us</span>
        <span>Contributor</span>
        </div>
        <div className="ml-72 grid mt-6 ">
        <span>Contact us</span>
        <span>(+254) 0768000000</span>
        <span>(+254) 0758000000</span>
        <span>Info@funzi.com</span>
        <span>Kilifi, Kenya</span>
        </div>
        <div className="ml-48 relative grid mt-6 ">
          <span>Terms and conditions</span>
          <span className='-mt-24'>Faq</span>
        </div>
         </div>
         <div className="bg-black text-white font-bold flex items-center justify-between px-10 py-4">
          <div className="space-x-80 ">
          <span>All Right Reserved | FUNZAI 2023</span>
          <span className="ml-64">Privacy Policy</span>
         </div>
          <span className="ml-36">Site Credit</span>
        </div>


         </>
  )
}

export default Footer 