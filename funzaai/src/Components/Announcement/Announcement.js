import React from 'react'
import { FormGroup } from 'react-bootstrap'

const Announcement = () => {
  return (
    <div>
        <span>Settings</span>
        <span>Basic Information</span>
        <span>Card Information</span>
        <span>Announcement</span>
        <div className="">
            < FormGroup required >
            < FormControlLabel  control={< Checkbox defaultChecked/> } label="Weekly Newsletter Subscription" />
            < FormControlLabel control={< Checkbox /> } label="New Uploaded Courses" />
            < FormControlLabel control={< Checkbox defaultChecked/>} label="New Freebies" />
            < FormControlLabel control={< Checkbox defaultChecked/> } label="Promotions" />
            </FormGroup>
            < Button desription="Save Changes" />
        </div>
    </div>
  )
}

export default Announcement