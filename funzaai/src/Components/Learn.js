import React from 'react'
import Button from '../Container/Button'
import { GoPlay } from "react-icons/go";

const Learn = () => {
  return (
    <>
    <div className="w-full h-[698px] text-white" style={{ background: 'linear-gradient(to right, #FFC000, #FF8A00)' }}>
    <div className='ml-8'>
      <div className="pt-24">
        <span className='font-bold text-3xl'>Learn Smarter, Not Harder, with</span>
        <br />
        <span className='font-bold text-3xl'>Blockchain Magic!</span>
        </div>
        < br />
        <div className="grid gap-4 mt-12 ">
        <span><b>Certified Magic: </b><i>Get blockchain-verified certificates that are as real as a wizard's spellbook!</i></span>
        <span><b>Enroll in a Snap:</b><i> With smart, secure enrollment, it's easier than waving a wand!</i></span>
        <span><b>AI Learning, the Fun Way: </b> <i>Our AI-powered learning is like having a friendly robot tutor by your side.</i></span>
        <span><b> Global Learning Party:</b> <i>Join a worldwide community of learners and let the fun times roll!</i></span>
        <div className="pt-24">
        < Button className="bg-white" description="Enroll Now" />
        </div>
        </div>
        <div className="flex mt-12 -gap-32 -space-x-36 -ml-24">
        <GoPlay className='bg-yellow-300, w-64 h-12'/>
        <GoPlay className='bg-yellow-300, w-64 h-14'/>
        </div>
        </div>
    </div>
    </>
  )
}

export default Learn