import React from 'react'
import Button from '../Container/Button'
import { FiShoppingCart } from "react-icons/fi";


const Navbar = () => {
  return (
    <>
    <div className="flex ml-4 mt-6">
    <div className="font-bold text-3xl">
        <span>FunzAI</span>
    </div>
    <div className='gap-6 ml-3 flex mt-2'>
        <span>Home</span>
        <span>Courses</span>
        <span>About Us</span>
        <span>Contact Us</span>
        <FiShoppingCart style={{width: '48px',  height: '96px', color: '', marginTop: '-28px'}}/>
        <div className="flex gap-8 -mt-2">
        < Button description ="Be a contributor" />
        < Button description ="Login" />
        

        </div>
    </div>
    </div>
    </>
  )
}

export default Navbar