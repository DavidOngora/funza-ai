import React from 'react'

const Confirm = () => {
  return (
    <div>
        <span>Your product code</span>
        <span>23110051</span>
        <span>Copy code 23110051 to proceed to Mpesa payment</span>
        <span>Follow the “how it works” process for better understanding of how the payment gateway works</span>
        <span>How it works?</span>
        <span>Go to Mpesa menu and click Lipa na Mpesa Paybill</span>
        <span>Enter 3330000 as FunzAI business number</span>
        <span>Enter Product code as the account number</span>
        <span>Input amount to be paid</span>
        <span>Input your PIN number</span>
        <span>Wait for payment confirmation message </span>
    </div>
  )
}

export default Confirm