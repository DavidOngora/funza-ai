import React from 'react'

const ConnectWallet = () => {
  return (
    <div>
        <span>Sign in with one of available wallet providers or create a new wallet.</span>
        < Button description="Metamask" />
        < Button description="Coinbase" />
        < Button description="Wallet connect" />
        < Button description="Crypto.com" />
        < Button description="Connect" />
    </div>
  )
}

export default ConnectWallet