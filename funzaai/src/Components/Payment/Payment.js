import React from 'react'
import { FormGroup } from 'react-bootstrap'

const Payment = () => {
  return (
    <>
    <div>
        <span>Confirm Your Preferred Payment Method</span>
        < FormGroup >
            < FormControl required control ={ < Checkbox defaultChecked /> } label="Pay with Mpesa" />
            <FormControlLabel required control={<Checkbox />} label="Pay with Wallet" />
        </FormGroup>
            < Button description="Next" />
    </div>
    < Footer />
    </>
  )
}

export default Payment