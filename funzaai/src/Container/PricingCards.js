import React from 'react'
import { IoCheckmarkCircle } from "react-icons/io5";
import { GoDotFill } from "react-icons/go";


const PricingCards = ({plan, amount, description, propers}) => {
  return (
    <>
    <div>
        <span>{plan}</span>
        <span className='font-bold text-[50px]'>${amount}</span>
        <span><GoDotFill />{description}</span>
        <span><IoCheckmarkCircle />{propers}</span>

    </div>
    </>
  )
}

export default PricingCards