import React from 'react'

const Button = ({description}) => {
  return (
    <div>
        < button className="rounded-full bg-blend-overlay bg-yellow-600 text-14 text-lg font-bold text-white w-[160px] h-[56px]">{description}</button>
    </div>
  )
}

export default Button